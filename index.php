<?php

function set($strStart, $strEnd)
{
    $template = ['b', 'c', 'd', 'f', 'm', 'n', 'p', 'q', 'w', 'x', 'y', 'z'];

    if (empty($strStart) || count(array_diff(str_split($strStart), $template))) {
        throw new Exception('First string not correct');
    }

    if (empty($strEnd) || count(array_diff(str_split($strEnd), $template))) {
        throw new Exception('Second string not correct');
    }

    $set = [];
    if (diff($strStart, $strEnd, $template) > 0) {
        $event = 'incrementLetter';
    } else {
        $event = 'decrementLetter';
    }

    $set[] = $strStart;
    while ($strStart !== $strEnd) {
        $event($strStart, 1, $template);
        $set[] = $strStart;
    };

    return $set;
}

function diff($strStart, $strEnd, $template)
{
    $diff = strlen($strEnd) - strlen($strStart);
    if (!$diff) {
        for ($i = 0; $i < strlen($strStart); $i++) {
            $diff = array_search($strEnd[$i], $template) - array_search($strStart[$i], $template);
            if ($diff) {
                return $diff;
            }
        }
    }
    return $diff;
}

function incrementLetter(&$strStart, $pointer, $template)
{
    $char = $strStart[strlen($strStart) - $pointer];
    $indexChar = array_search($char, $template);

    if (strlen($strStart) - $pointer === 0 && $indexChar === (count($template) - 1)) {
        $strStart[strlen($strStart) - $pointer] = getFirstElement($template);
        $strStart .= getFirstElement($template);
    } elseif ($indexChar === (count($template) - 1)) {
        $strStart[strlen($strStart) - $pointer] = getFirstElement($template);
        incrementLetter($strStart, $pointer + 1, $template);
    } else {
        $strStart[strlen($strStart) - $pointer] = $template[$indexChar + 1];
    }
}

function decrementLetter(&$strStart, $pointer, $template)
{
    $char = $strStart[strlen($strStart) - $pointer];
    $indexChar = array_search($char, $template);
    if (strlen($strStart) - $pointer === 0 && strlen($strStart) !== 1) {
        $strStart[0] = getLastElement($template);
        $strStart = substr($strStart, 0, strlen($strStart) - 1);
    } elseif ($indexChar === 0) {
        $strStart[strlen($strStart) - $pointer] = getLastElement($template);
        decrementLetter($strStart, $pointer + 1, $template);
    } else {
        $strStart[strlen($strStart) - $pointer] = $template[$indexChar - 1];
    }

}

function getFirstElement($array)
{
    return $array[0];
}

function getLastElement($array)
{
    return $array[count($array) - 1];
}

?>

<pre>
    <?php
    var_dump(set('bd', 'b'));
    var_dump(set('b', 'bd'));
    ?>
</pre>
