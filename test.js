// Write in JS function which takes 1 object argument (any object, native or user-defined)
// and returns new object which has the same methods and acts as argument, while each method
// call is logged in browser console.
//     For example:
//
//     var a = [];
// var b = myfunc(a);
// b.push("test");
// < array.push("test") -> 1;
// b.push("test2");
// < array.push("test2") -> 2;
// console.log(a);
// < ["test", "test2"]

function getFunction(object) {
    if (object.constructor.name !== 'Object') {
        object = object.constructor;
    }

    return Object.getOwnPropertyNames(object).filter(function (key) {
        return typeof object[key] === 'function';
    });
}

function myfunc(objectImport) {
    let counter = 0;
    const functions = getFunction(objectImport);
    const object = {};
    for (let fun of functions){
        object[fun] = (...arg) => {
            objectImport[fun](...arg);
            console.log(`< ${objectImport.constructor.name}.${fun}(${arg.join(', ')}) -> ${++counter}`);
        }
    }
    return object;
}

const a = [];
    
const b = myfunc(a);

b.push('test1', 'test3');
b.push('test2');
